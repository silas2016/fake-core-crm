package entities

import (
	"fmt"
	"strings"
)

type Customer struct {
	ID     int
	Name   string
	Cpf    string
	Gender string
	Emails []Email
	Phones []Phone
}

func MakeCustomer(name string, cpf string, gender string, emails []Email, phones []Phone) (Customer, error) {
	c := Customer{
		Name:   name,
		Cpf:    cpf,
		Gender: gender,
		Emails: emails,
		Phones: phones,
	}

	err := c.validate()
	if err != nil {
		return c, err
	}

	return c, nil
}

func (c *Customer) validate() error {
	err := c.validateName()
	if err != nil {
		return err
	}

	err = c.validateCpf()
	if err != nil {
		return err
	}

	err = c.validateEmails()
	if err != nil {
		return err
	}

	err = c.validatePhones()
	if err != nil {
		return err
	}

	return nil
}

func (c *Customer) validateName() error {
	if len(c.Name) == 0 {
		return fmt.Errorf("O name deve ser preenchido")
	}

	return nil
}

func (c *Customer) validateCpf() error {
	if len(c.Cpf) == 0 {
		return fmt.Errorf("O cpf deve ser prenchido")
	}

	if len(strings.ReplaceAll(strings.ReplaceAll(c.Cpf, ".", ""), "-", "")) != 11 {
		return fmt.Errorf("O cpf deve ter 11 dígitos")
	}

	return nil
}

func (c *Customer) validateEmails() error {
	for i := 0; i < len(c.Emails); i++ {
		err := c.Emails[i].validate()
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Customer) validatePhones() error {
	for i := 0; i < len(c.Phones); i++ {
		p := c.Phones[i]
		err := p.validate()
		if err != nil {
			return err
		}
	}

	return nil
}
