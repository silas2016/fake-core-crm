package entities

import (
	"fmt"
	"strings"
)

type Email struct {
	ID         int
	Email      string
	CustomerID int
}

func MakeEmail(email string) (Email, error) {
	e := Email{Email: email}
	err := e.validate()
	if err != nil {
		return e, err
	}

	return e, nil
}

func (e *Email) validate() error {
	if len(strings.Trim(e.Email, "")) == 0 {
		return fmt.Errorf("O email deve ser preenchido")
	}

	if strings.Index(e.Email, "@") < 1 && len(strings.Trim(e.Email, "")) < 3 {
		return fmt.Errorf("O email é inválido")
	}

	return nil
}
