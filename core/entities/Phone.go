package entities

import (
	"fmt"
	"strings"
)

type Phone struct {
	ID         int
	Number     string
	CustomerID int
}

func MakePhone(number string) (Phone, error) {
	p := Phone{
		Number: number,
	}

	err := p.validate()
	if err != nil {
		return p, err
	}

	return p, nil
}

func (p *Phone) validate() error {
	if len(strings.Trim(p.Number, "")) == 0 {
		return fmt.Errorf("O number deve ser preenchido")
	}

	if len(strings.Trim(p.Number, "")) < 10 {
		return fmt.Errorf("O number deve ter no mínimo 10 dígitos")
	}

	return nil
}
