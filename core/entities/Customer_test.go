package entities

import "testing"

func TestShouldCreateCustomer(t *testing.T) {
	emails := make([]Email, 0)
	emails = append(emails, Email{
		Email: "silas.silva@digitalk.com.br",
	})
	phones := make([]Phone, 0)
	phones = append(phones, Phone{
		Number: "11912347894",
	})

	c, err := MakeCustomer("Silas", "12345678910", "masculino", emails, phones)
	if err != nil {
		t.Errorf("got %q, want nil", err)
	}

	wantName := "Silas"

	if c.Name != wantName {
		t.Errorf("got %q, want %q", c.Name, wantName)
	}
}
