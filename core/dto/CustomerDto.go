package dto

import "gitlab.com/silas2016/fake-core-crm/core/entities"

type CustomerDto struct {
	ID     int        `json:"id,omitempty"`
	Name   string     `json:"name"`
	Cpf    string     `json:"cpf"`
	Gender string     `json:"gender"`
	Phones []PhoneDto `json:"phones"`
	Emails []EmailDto `json:"emails"`
}

func (cd *CustomerDto) ToCustomerEntity() (entities.Customer, error) {
	var customer entities.Customer

	emails, err := ToListEmailEntity(cd.Emails)
	if err != nil {
		return customer, err
	}

	phones, err := ToListPhoneEntity(cd.Phones)
	if err != nil {
		return customer, err
	}

	customer, err = entities.MakeCustomer(cd.Name, cd.Cpf, cd.Gender, emails, phones)
	if err != nil {
		return customer, err
	}

	return customer, nil
}

func MakeCustomerDtoFromCustomerEntity(customer entities.Customer) CustomerDto {
	customerDto := CustomerDto{
		ID:     customer.ID,
		Name:   customer.Name,
		Cpf:    customer.Cpf,
		Gender: customer.Gender,
		Emails: MakeListEmailDtoFromListEmailEntity(customer.Emails),
		Phones: MakeListPhoneDtoFromListPhoneEntity(customer.Phones),
	}

	return customerDto
}
