package dto

import "gitlab.com/silas2016/fake-core-crm/core/entities"

type PhoneDto struct {
	ID     int    `json:"id,omitempty"`
	Number string `json:"number"`
}

func (pd *PhoneDto) ToPhoneEntity() (entities.Phone, error) {
	p, err := entities.MakePhone(pd.Number)
	if err != nil {
		return p, err
	}

	return p, nil
}

func ToListPhoneEntity(phonesDto []PhoneDto) ([]entities.Phone, error) {
	phones := make([]entities.Phone, 0)

	for i := 0; i < len(phonesDto); i++ {
		p, err := phonesDto[i].ToPhoneEntity()
		if err != nil {
			return phones, err
		}
		phones = append(phones, p)
	}

	return phones, nil
}

func MakePhoneDtoFromPhoneEntity(phone entities.Phone) PhoneDto {
	return PhoneDto{
		ID:     phone.ID,
		Number: phone.Number,
	}
}

func MakeListPhoneDtoFromListPhoneEntity(phones []entities.Phone) []PhoneDto {
	phonesDto := make([]PhoneDto, 0)

	for i := 0; i < len(phones); i++ {
		phonesDto = append(phonesDto, MakePhoneDtoFromPhoneEntity(phones[i]))
	}

	return phonesDto
}
