package dto

import "gitlab.com/silas2016/fake-core-crm/core/entities"

type EmailDto struct {
	ID    int    `json:"id,omitempty"`
	Email string `json:"email"`
}

func (ed *EmailDto) ToEmailEntity() (entities.Email, error) {
	email, err := entities.MakeEmail(ed.Email)
	if err != nil {
		return email, err
	}

	return email, nil
}

func ToListEmailEntity(emailsDto []EmailDto) ([]entities.Email, error) {
	emails := make([]entities.Email, 0)
	for i := 0; i < len(emailsDto); i++ {
		email, err := emailsDto[i].ToEmailEntity()
		if err != nil {
			return emails, err
		}
		emails = append(emails, email)
	}

	return emails, nil
}

func MakeEmailDtoFromEmailEntity(email entities.Email) EmailDto {
	emailDto := EmailDto{
		ID:    email.ID,
		Email: email.Email,
	}

	return emailDto
}

func MakeListEmailDtoFromListEmailEntity(emails []entities.Email) []EmailDto {
	emailsDto := make([]EmailDto, 0)

	for i := 0; i < len(emails); i++ {
		emailsDto = append(emailsDto, MakeEmailDtoFromEmailEntity(emails[i]))
	}

	return emailsDto
}
