package services

import (
	"fmt"

	"gitlab.com/silas2016/fake-core-crm/core/dto"
	"gitlab.com/silas2016/fake-core-crm/core/entities"
)

type ICustomerRepository interface {
	Save(customer entities.Customer) (entities.Customer, error)
}

type IPhoneRepository interface {
	SaveBatch(phones []entities.Phone, customerID int) ([]entities.Phone, error)
}

type IEmailRepository interface {
	SaveBatch(emails []entities.Email, customerID int) ([]entities.Email, error)
}

type ISMSBroker interface {
	SendMessage(message string, phone string) error
}

type CustomerService struct {
	CustomerRepo ICustomerRepository
	PhoneRepo    IPhoneRepository
	EmailRepo    IEmailRepository
	SMSBroker    ISMSBroker
}

func MakeCustomerService(csRepo ICustomerRepository, pRepo IPhoneRepository, eRepo IEmailRepository, smsBroker ISMSBroker) CustomerService {
	return CustomerService{
		CustomerRepo: csRepo,
		PhoneRepo:    pRepo,
		EmailRepo:    eRepo,
		SMSBroker:    smsBroker,
	}
}

func (cs *CustomerService) CreateCustomer(customerDto dto.CustomerDto) (dto.CustomerDto, error) {
	customer, err := customerDto.ToCustomerEntity()
	if err != nil {
		return customerDto, err
	}

	customer, err = cs.CustomerRepo.Save(customer)
	if err != nil {
		return customerDto, err
	}

	customer.Emails, err = cs.EmailRepo.SaveBatch(customer.Emails, customer.ID)
	if err != nil {
		return customerDto, err
	}

	customer.Phones, err = cs.PhoneRepo.SaveBatch(customer.Phones, customer.ID)
	if err != nil {
		return customerDto, err
	}

	customerDto = dto.MakeCustomerDtoFromCustomerEntity(customer)

	err = cs.sendGreetingMessage(customer)
	if err != nil {
		return customerDto, err
	}

	return customerDto, nil
}

func (cs *CustomerService) sendGreetingMessage(customer entities.Customer) error {
	for i := 0; i < len(customer.Phones); i++ {
		message := fmt.Sprintf("Olá %s seja bem vindo!", customer.Name)
		err := cs.SMSBroker.SendMessage(message, customer.Phones[i].Number)
		if err != nil {
			return err
		}
	}

	return nil
}
