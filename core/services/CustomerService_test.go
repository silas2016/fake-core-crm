package services

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/silas2016/fake-core-crm/core/dto"
	"gitlab.com/silas2016/fake-core-crm/core/entities"
)

type CustomerRepoMock struct{}
type EmailRepoMock struct{}
type PhoneRepoMock struct{}
type SMSBrokerMock struct{}

func (crm *CustomerRepoMock) Save(customer entities.Customer) (entities.Customer, error) {
	customer.ID = int(time.Now().UnixNano())
	return customer, nil
}

func (erm *EmailRepoMock) SaveBatch(emails []entities.Email, customerID int) ([]entities.Email, error) {
	for i := 0; i < len(emails); i++ {
		emails[i].ID = int(time.Now().UnixNano())
		emails[i].CustomerID = customerID
	}

	return emails, nil
}

func (prm *PhoneRepoMock) SaveBatch(phones []entities.Phone, customerID int) ([]entities.Phone, error) {
	for i := 0; i < len(phones); i++ {
		phones[i].ID = int(time.Now().UnixNano())
		phones[i].CustomerID = customerID
	}

	return phones, nil
}

func (sbm *SMSBrokerMock) SendMessage(message string, phone string) error {
	fmt.Printf("Sending message: %s to phone %s\n", message, phone)

	return nil
}

func TestShouldCreateCustomerAndSendMessage(t *testing.T) {
	emailsDto := make([]dto.EmailDto, 0)
	emailsDto = append(emailsDto, dto.EmailDto{
		Email: "silas.silva@digitalk.com.br",
	})

	phonesDto := make([]dto.PhoneDto, 0)
	phonesDto = append(phonesDto, dto.PhoneDto{
		Number: "11912347894",
	})

	customerDto := dto.CustomerDto{
		Name:   "Silas",
		Cpf:    "12345678910",
		Gender: "masculino",
		Phones: phonesDto,
		Emails: emailsDto,
	}

	customerRepo := CustomerRepoMock{}
	emailRepo := EmailRepoMock{}
	phoneRepo := PhoneRepoMock{}
	smsBroker := SMSBrokerMock{}

	customerService := MakeCustomerService(&customerRepo, &phoneRepo, &emailRepo, &smsBroker)

	gotCustomerDto, err := customerService.CreateCustomer(customerDto)
	if err != nil {
		t.Errorf("got %q, want nil", err)
	}

	if gotCustomerDto.ID == 0 {
		t.Errorf("got %d, want > 0", gotCustomerDto.ID)
	}

	if gotCustomerDto.Emails[0].ID == 0 {
		t.Errorf("got %d, want > 0", gotCustomerDto.Emails[0].ID)
	}

}
